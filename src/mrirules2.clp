;; import the models to work with.
(import pfg.models.*)

;; get the templates from their Java classes
(deftemplate Question (declare(from-class Question)))
(deftemplate Recommendation (declare(from-class Recommendation)))

;; If the user answeres "NO" to adding shifts
;; then
;; Adding shifts is not ok.
(defrule rule1
    (Question (id "staff1")(answer "NO"))
    =>
    (assert (adding-shifts-nok)))

;; If the user answeres "YES" to adding shifts
;; and the user is willing to hire more than two radiologists
;; then
;; Adding shifts is ok.
(defrule rule2
    (Question (id "staff1")(answer "YES"))
    (Question (id "staff2"){numAnswer > 2})
    =>
    (assert (adding-shifts-ok)))

;; If more than 50 people need MRIs
;; and less than 66.6% of the patients need a scan with MRI contrast
;; and the hospital is in a province where less than 40% of the people are over the age of 65
;; and less than half of the patients are willing to go to another clinic
;; then
;; It is ok to open a 24H schedule according to the data from the patients.
(defrule rule3
    (Question (id "patients1"){numAnswer > 50})
    (Question (id "patients2"){numAnswer < 66.6})
    (Question (id "patients3"){numAnswer < 40})
    (Question (id "patients4"){numAnswer < 50})
    =>
    (assert (24h-patients-ok)))

;; If less than a 100 patients need MRI scans
;; then
;; It is not ok to open a 24H schedule according to the data from the patients.
(defrule rule4
    (Question (id "patients1"){numAnswer < 100})
    =>
    (assert (24h-patients-nok)))

;; If more than 66.6% of patients need MRI scans
;; then
;; It is not ok to open a 24H schedule according to the data from the patients.
(defrule rule5
    (Question (id "patients2"){numAnswer > 66.6})
    =>
    (assert (24h-patients-nok)))

;; If the hospital is in a province where more than 40% of the people are over the age of 65
;; then
;; It is not ok to open a 24H schedule according to the data from the patients.
(defrule rule6
    (Question (id "patients3"){numAnswer > 40})
    =>
    (assert (24h-patients-nok)))

;; If more than half of the patients are willing to go to another clinic
;; then
;; It is not ok to open a 24H schedule according to the data from the patients.
(defrule rule7
    (Question (id "patients4"){numAnswer > 50})
    =>
    (assert (24h-patients-nok)))

;; If the doctors are ok with externalised tests
;; and they don't usually repeat them
;; then
;; Doctors are ok with externalising.
(defrule rule8
    (Question (id "doctors1")(answer "YES"))
    (Question (id "doctors2")(answer "NO"))
    =>
    (assert (ext-doctors-ok)))

;; If the doctors are ok with externalised tests
;; but they usually repeat them
;; then
;; Doctors are MAYBE ok with externalising.
(defrule rule9
    (Question (id "doctors1")(answer "YES"))
    (Question (id "doctors2")(answer "YES"))
    =>
    (assert (ext-doctors-maybe)))

;; If the doctors are NOT ok with externalised tests
;; then
;; Doctors are NOT ok with externalising.
(defrule rule10
    (Question (id "doctors1")(answer "NO"))
    =>
    (assert (ext-doctors-nok)))

;; If the user is ok with opening the scan room at weekends
;; then
;; adding weekends is ok.
(defrule rule11
    (Question (id "schedule2")(answer "YES"))
    =>
    (assert (weekends-ok)))

;; If it is not ok to add shfits
;; and the doctors are not ok with externalising
;; then
;; Recommend that they evaluate some of their answers
(defrule rec0
    (not(adding-shifts-ok))
    (ext-doctors-nok)
    =>
     (add (new Recommendation "R0" "The system cannot give you a recommendation to externalise or add shifts based on your answers, think about changing some of them.")))


;; If adding shifts is not ok
;; and the doctors are ok with externalising
;; then
;; Recommend that they externalise and send the patients to an external clinic.
(defrule rec1
    (adding-shifts-nok)
    (ext-doctors-ok)
    =>
    (add (new Recommendation "Externalise1" "Externalise the MRI service and send the patients to a different clinic.")))

;; If adding shifts is not ok
;; and the doctors are maybe ok with externalising
;; then
;; Recommend that they externalise and send the patients to an external clinic.
(defrule rec2
    (adding-shifts-nok)
    (ext-doctors-maybe)
    =>
     (add (new Recommendation "Externalise2" "Consider externalising the MRI service but review your options.")))

;; If adding shifts is ok
;; and it is ok to have 24h according to the patients' data
;; then
;; Open up the scan room 24 hours.
;; NOTE: THIS IS THE IDEAL SOLUTION.
(defrule rec3
    (adding-shifts-ok)
    (24h-patients-ok)
    (Question (id "schedule1")(answered TRUE)(answer "00:00"))
    =>
     (add (new Recommendation "24h" "Open the scan room 24h.")))

;; If adding shifts is ok
;; and it is ok to have 24h shifts according to the patients' data
;; but the user is not willing to open the scan room until midnight
;; then
;; Recommend that they open the scan room until the time the user input
(defrule rec4
    (adding-shifts-ok)
    (24h-patients-ok)
    (Question (id "schedule1")(answered TRUE)(answer ?max-time&:(neq ?max-time "00:00")))
    =>
     (add (new Recommendation "AddShifts" (str-cat "Open the scan room until " ?max-time))))

;; If adding shifts is ok
;; and it is not ok to have 24h shifts according to the patients' data
;; and doctors are not ok with externalising
;; and the user is not willing to open the scan room until midnight
;; then
;; Recommend that they open the scan room until the time the user input
(defrule rec5
    (adding-shifts-ok)
    (24h-patients-nok)
    (ext-doctors-nok)
    (Question (id "schedule1")(answered TRUE)(answer ?max-time&:(neq ?max-time "00:00")))
    =>
     (add (new Recommendation "AddShifts" (str-cat "Open the scan room until " ?max-time))))

;; If the user answered anything besides "NO" to the question...
;; ...whether they're willing to share patient information with external employees
;; then
;; they should read about GDPR.
(defrule rec6
    (Question (id "medhis1")(answered TRUE)(answer ~"NO"))
    =>
    (add (new Recommendation "readGDPR" "Read about data protection laws.")))

;; If the user answered anything besides "24" to the question...
;; ...for how many hours they're willing to keep the MRI machine running
;; then
;; they should read documentation about the MRI machine.
(defrule rec7
    (Question (id "machine1")(answered TRUE)(answer ~"24"))
    =>
    (add (new Recommendation "readMRI" "Read about the MRI scan machine.")))

;; If adding shifts is OK
;; But it's not ok to have 24h shifts from the patient data
;; and the doctors are not ok with externalising
;; but the user is ok with opening at weekends
;; then
;; Recommend that they open up weekend shifts.
(defrule rec8
    (adding-shifts-ok)
    (24h-patients-nok)
    (ext-doctors-nok)
    (weekends-ok)
    =>
    (add (new Recommendation "AddWeekends" "Add weekend shifts.")))

;; if the user has not answered the question about IV contrast
;; then
;; recommend that they read about IV contrast.
(defrule rec9
    (Question (id "patients2")(answered FALSE))
    =>
    (add (new Recommendation "readContrast" "Read about IV contrast.")))

;; if the user hasn't answered the question about the population in the province
;; then
;; recommend that they look up the population in their province
(defrule rec10
    (Question (id "patients3")(answered FALSE))
    =>
    (add (new Recommendation "readPopulation" "Find out the population in your province.")))

;; If every single question has been answered
;; then
;; Offer the user the case study used to create this platform.
(defrule answeredall
    (not(Question (answered FALSE)))
    =>
    (add (new Recommendation "readCaseStudy" "You have answered all the questions. Read the Case Study.")))