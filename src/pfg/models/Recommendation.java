/**
 * <h1> Recommendation </h1>
 * 
 * Java class that models the recommendations the system can give the user.
 * @author Glenn AWB
 * @version 1.0
 */
package pfg.models;

public class Recommendation {

	private String id;
	private String text;
	
	/**
	 * Creates a recommendation with a specific ID and text.
	 * 
	 * @param id the ID of the recommendation
	 * @param text the text of the recommendation.
	 */
	public Recommendation(String id, String text) {
		this.id = id;
		this.text = text;
	}
	
	/**
	 * Returns the ID of the recommendation.
	 * 
	 * @return A String containing the ID of the recommendation.
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Returns the text of the recommendation.
	 * @return A String containing the text of the recommendation.
	 */
	public String getText() {
		return text;
	}
	
}
