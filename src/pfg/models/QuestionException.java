/**
 * <h1> Question Exception </h1>
 * Exception to be thrown in case there is an error when creating or answering a question.
 * @author Glenn AWB
 * @version 1.0
 */

package pfg.models;

public class QuestionException extends Exception {
	
	private static final long serialVersionUID = 1319199977048256622L;

	/**
	 * Sole constructor for this exception.
	 * 
	 * @param message A String containing the message giving an explanation of the error.
	 */
	public QuestionException(String message) {
		super(message);
	}

}
