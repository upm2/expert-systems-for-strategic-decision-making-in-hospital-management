/**
 * <h1> Question Model </h1>
 * 
 * Models the types of questions (and answers they accept) that the Expert System can use to generate recommendations.
 * 
 * @author Glenn AWB
 * @version 1.0
 */
package pfg.models;

import java.util.Arrays;

public class Question {
	
	//Question types
	
	/**
	 * A yes/no question (answer can be don't know).
	 */
	public static final int TYPE_YES_NO = 0;
	/**
	 * A multiple choice question.
	 */
	public static final int TYPE_MULTCHOICE = 1;
	/**
	 * A number question.
	 */
	public static final int TYPE_NUMBER = 2;
	
	//private Question attributes
	private String id;
	private boolean answered;
	private String question;
	private int type;
	String answer;
	
	String p_answers []; //possible answers ONLY MULTIPLE CHOICE
	
	/**
	 * The constructor used for yes/no questions or number type questions.
	 * 
	 * @param id The ID of the question.
	 * @param question The question to be asked.
	 * @param type The type of question, either yes/no or number.
	 * @throws QuestionException If the user specifies the multiple choice type.
	 */
	public Question(String id, String question, int type) throws QuestionException {
		this.answered = false;
		this.question = question;
		this.type = type;
		answer = null;
		this.id = id;
		
		switch(type) {
		case TYPE_YES_NO:
			p_answers = new String[] {"YES", "NO", "DON'T KNOW"};
			break;
		case TYPE_MULTCHOICE:
			throw new QuestionException("A multiple choice question needs to be created with its possible answers");
		case TYPE_NUMBER:
			p_answers = new String[] {"NUMBER VALUE"};
			break;
		default:
			throw new QuestionException("Invalid question type.");
		}
	}
	
	/**
	 * The constructor to be used for multiple choice questions.
	 * 
	 * @param id The ID of the question
	 * @param question The question to be asked.
	 * @param type The type of question, must be multiple choice.
	 * @param choices a String array containing the possible answers for the question.
	 * @throws QuestionException if the user specifies a type that is not multiple choice.
	 */
	public Question(String id, String question, int type, String[] choices) throws QuestionException {
		this.answered = false;
		this.question = question;
		this.type = type;
		answer = null;
		this.id = id;
		
		switch(type) {
		case TYPE_MULTCHOICE:
			p_answers = choices;
			break;
		default:
			throw new QuestionException("Invalid constructor for question type.");
		}
	}

	/**
	 * A method used to determine if a question has been answered.
	 * 
	 * @return a boolean specifying whether the question has been answered (true) or not (false).
	 */
	public boolean getAnswered() {
		return answered;
	}

	/**
	 * Returns the question to be asked to the user.
	 * 
	 * @return a String containing the question text.
	 */
	public String getQuestion() {
		return question;
	}
	
	/**
	 * Returns the type of question.
	 * 
	 * @return an integer value matching one of the question types.
	 */
	public int getType() {
		return type;
	}
	
	/**
	 * The method used to answer a question. The answer must be valid according to the type of question.
	 * 
	 * @param answer a String containing the answer.
	 * @throws QuestionException if an invalid answer was given.
	 */
	public void answerQuestion(String answer) throws QuestionException {
		
		if ((type == TYPE_YES_NO || type == TYPE_MULTCHOICE) && Arrays.asList(p_answers).contains(answer)) { 
			answered = true;
			this.answer = answer;
		}else if (type == TYPE_NUMBER && answer.matches("[-+]?\\d+(\\.\\d+)?")) {
			answered = true;
			this.answer = answer;
		}else {
			throw new QuestionException("Invalid answer.");
		}
		
		
	}
	
	/**
	 * Returns the current answer to the question.
	 * 
	 * @return the a String containing the answer to the question or "NOT_ANSWERED" if it hasn't been answered.
	 */
	public String getAnswer() {
		if(answered) {
			return answer;
		}else {
			return "NOT_ANSWERED";
		}
	}
	
	/**
	 * Returns the answer to a number type question.
	 * 
	 * @return the current answer to the question, or a value of -1 if it is not a number type question or hasn't been answered.
	 */
	public double getNumAnswer() {
		double res;
		if(type == TYPE_NUMBER && answered) {
			res = Double.parseDouble(answer);
			return res;
		}else {
			return -1;
		}
	}
	
	/**
	 * Returns the ID of the question.
	 * 
	 * @return a String containing the ID of the question.
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Returns all the possible answers of a question.
	 * 
	 * @return a String array containing "YES", "NO" and "DON'T KNOW" if it's a yes/no question,
	 * a String array containing all the possible answers to a multiple choice question,
	 * or a String array containing a single element ("NUMBER TYPE") if it's a number type question.
	 */
	public String[] getPossibleAnswers() {
		return p_answers;
	}
}
