/**
 * <h1> MRI Engine (Jess) </h1>
 * 
 * The MRIEngine class implements the Jess script used for decision making.
 *
 * @author Glenn AWB
 * @version 1.0
 */
package pfg.mriengine;

import java.util.Iterator;
import jess.*;
import pfg.models.*;

public class MRIEngine {

	private Rete engine;
	private WorkingMemoryMarker marker;
	private QuestionsDb qdb;
	
	/**
	 * Sole constructor of the class.
	 * 
	 * @param qdb The Collection of Questions used to generate Recommendations.
	 * @throws JessException If there is an error in the Jess script.
	 */
	public MRIEngine(QuestionsDb qdb) throws JessException {
		this.qdb = qdb;
		
		engine = new Rete();
		engine.reset();        
        //Mark starting point
        marker = engine.mark();
	}
	
	/**
	 * Loads the question into the Jess memory.
	 * 
	 * @throws JessException if a problem occurs in Jess
	 */
	public void load() throws JessException {
		engine.addAll(qdb.getQuestions());
	}
	
	/**
	 * Runs the engine and generates recommendations, which are stored along the questions.
	 * 
	 * @param script the location of the script file
	 * @return the number of recommendations generated
	 * @throws JessException If a problem occurs in Jess
	 */
	@SuppressWarnings("unchecked") //It is fine, it filters by class. 
	public int run(String script) throws JessException{
		//clear recommendations
		qdb.clearRecommendations();
		
		//go back to a clean syste
		engine.resetToMark(marker);
		 // Load the rules for the system
        engine.batch(script);
		//load questions
		load();
		
		//fire the rules
		engine.run();
		int count = 0;
		
		Iterator<Recommendation> i 
			= engine.getObjects(new Filter.ByClass(Recommendation.class));
		
		while(i.hasNext()) {
			count++;
			qdb.addRecommendation(i.next());
		}
		
		return count;
	}
}
