/**
 * 
 * <h1> Questions Database </h1>
 * 
 * The QuestionsDb interface defines the methods needed to provide the user with questions and recommendations.
 * 
 * @author Glenn AWB
 * @version 1.0
 */
package pfg.mriengine;

import java.util.Collection;
import pfg.models.Question;
import pfg.models.Recommendation;

public interface QuestionsDb {
	
	/**
	 * Provides all of the questions currently in the collection.
	 * 
	 * @return all the questions in the collection
	 */
	public Collection<Question> getQuestions();
	
	/**
	 * Provides all of the recommendations generated currently in the collection.
	 * 
	 * @return all of the recommendations in the collection
	 */
	public Collection<Recommendation> getRecommendations();
	
	/**
	 * Provides a single, specific question according to the ID provided.
	 * 
	 * @param id the ID of the question queried
	 * @return the question that matches that ID or null if none do
	 */
	public Question getQuestion(String id);
	
	/**
	 * Provides a single, specific recommendation according to the ID provided.
	 * 
	 * @param id the ID of the recommendation queried
	 * @return the question that matches that ID or null if none do
	 */
	public Recommendation getRecommendation(String id);
	
	/**
	 * Adds a recommendation to the collection.
	 * 
	 * @param rec the recommendation to be added
	 */
	public void addRecommendation(Recommendation rec);
	
	/**
	 * Deletes all of the recommendations in the collection.
	 */
	public void clearRecommendations();
}
