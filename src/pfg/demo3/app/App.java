/**
 * <h1> Expert System Application </h1>
 * 
 * Contains the main method that generates windows (GUI) and creates instances of the Expert System engine's components to run.
 * 
 * @author Glenn AWB
 */

package pfg.demo3.app;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Set;

import jess.JessException;
import pfg.demo3.db.DbManager;
import pfg.demo3.gui.MainWindow;
import pfg.models.Question;
import pfg.models.Recommendation;
import pfg.mriengine.MRIEngine;
import pfg.mriengine.QuestionsDb;

public class App {
	
	private String jessscript;

	private MRIEngine engine;
	private QuestionsDb dq;
	private DbManager db;
	
	/**
	 * Main class, runs the application.
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		if(args.length == 4) {
			App app = new App(args[0], args[1], args[2], args[3]);
			MainWindow mw = new MainWindow(app);
			mw.setVisible(true);
		}else {
			System.out.println("Provide the arguments for the program.");
		}
	}
	
	/**
	 * Constructor of the class.
	 * Initialises the components of the application.
	 */
	private App(String dbpath, String dbname, String qfile, String jessscript) {
		try {
			
			this.jessscript = jessscript;
			
			db = new DbManager(dbpath, dbname);
			dq = new CSV2Questions(qfile, ",");
			engine = new MRIEngine(dq);
			
		} catch (JessException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Opens a webpage obtained from the documentation database.
	 * 
	 * @param docId the document's ID
	 * @throws IOException  if the user default browser is not found
	 * @throws URISyntaxException If the given string violates RFC 2396, as augmented by the above deviations
	 * @throws SQLException If there is an error querying the database
	 */
	public void openWeb(String docId) throws IOException, URISyntaxException, SQLException {
		
		String url = db.getUrl(docId);
		
		if (Desktop.isDesktopSupported()) {
		    Desktop.getDesktop().browse(new URI(url));
		}
	}
	
	/**
	 * Opens a blob obtained from the database.
	 * 
	 * @param docId the document's ID
	 * @return the String containing the location of the file.
	 * @throws SQLException if there is a problem querying the database
	 */
	public String openBlob(String docId) throws SQLException {
		
		String destination = "";
		FileOutputStream fos = null;
		
		String type = db.getType(docId);
		InputStream is = db.getBlob(docId);
		
		if(!type.equals("URL")) {
			try {
				destination = File.createTempFile("mri", "."+type.toLowerCase()).getAbsolutePath();
				// write binary stream into file
		        File file = new File(destination);
		        fos = new FileOutputStream(file);
		        byte[] buffer = new byte[1024];
                while (is.read(buffer) > 0) {
                    fos.write(buffer);
                }
			} catch (IOException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			} finally {
				if (fos != null) {
                    try {
						fos.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
			}
		}
		
		if (Desktop.isDesktopSupported() && !destination.equals("")) {
		    try {
		        File myFile = new File(destination);
		        Desktop.getDesktop().open(myFile);
		    } catch (IOException e) {
				System.out.println(e.getMessage());
		    	e.printStackTrace();
		    }
		}
		return destination;
	}
	
	/**
	 * Runs the engine with the specified script.
	 * 
	 * @return the number of recommendations obtained.
	 * @throws JessException if there is an error in Jess.
	 */
	public int run() throws JessException {
		return engine.run(jessscript);
	}
	
	/**
	 * Returns the collection of questions in the system.
	 * 
	 * @return the collection of questions in the system.
	 */
	public Collection<Question> getQuestions() {
		return dq.getQuestions();
	}
	
	/**
	 * Returns a specific question according to its ID.
	 * @param id the ID of the question.
	 * @return the question that matches the ID.
	 */
	public Question getQuestion(String id) {
		return dq.getQuestion(id);
	}
	
	/**
	 * Returns the collection of recommendations generated in the system.
	 * 
	 * @return the collection of recommendations in the system.
	 */
	public Collection<Recommendation> getRecommendations() {
		return dq.getRecommendations();
	}
	
	/**
	 * Returns the IDs of all the documents that match a certain Recommendation ID from the Database.
	 * 
	 * @param recId the recommendation's ID
	 * @return the set of IDs that match the ID queried.
	 * @throws SQLException if there is a problem querying the database.
	 */
	public Set<String> getDocIdsfromRecId(String recId) throws SQLException {
		return db.getIdsFromRecommendation(recId);
	}
	
	/**
	 * Returns the name of the documents that match a certain Documentation ID from the Database.
	 * 
	 * @param dId the document's ID
	 * @return the set of IDs that match the ID queried.
	 * @throws SQLException if there is a problem querying the database.
	 */
	public String getDocName(String dId) throws SQLException {
		return db.getName(dId);
	}
	
	/**
	 * Returns the type of a documents that matches a certain Documentation ID from the Database.
	 * 
	 * @param dId the document's ID
	 * @return the type of the document whose IDs that match the ID queried.
	 * @throws SQLException if there is a problem querying the database.
	 */
	public String getDocType(String dId) throws SQLException {
		return db.getType(dId);
	}
	
	/**
	 * Returns the URL of a documents that matches a certain Documentation ID from the Database.
	 * 
	 * @param dId the document's ID
	 * @return the URL of the document whose IDs that match the ID queried.
	 * @throws SQLException if there is a problem querying the database.
	 */
	public String getDocUrl(String dId) throws SQLException {
		return db.getUrl(dId);
	}
}
