/**
 * <h1> CSV file to Questions database </h1>
 * 
 * Builds a collection of questions to be used with the MRI Engine from a CSV file.
 * 
 * @author Glenn AWB
 */
package pfg.demo3.app;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.TreeMap;

import pfg.models.Question;
import pfg.models.QuestionException;
import pfg.models.Recommendation;
import pfg.mriengine.QuestionsDb;

public class CSV2Questions implements QuestionsDb {

	private TreeMap<String, Question> questionmap;
	private TreeMap<String, Recommendation> recmap;
	
	/**
	 * Takes a CSV file and uses its contents to generate questions for the engine.
	 * 
	 * @param csvfile location of the CSV file.
	 * @param delim character used to delimit entries in the file.
	 */
	public CSV2Questions(String csvfile, String delim) {
		questionmap = new TreeMap<String, Question>();
		recmap = new TreeMap<String, Recommendation>();
		
		BufferedReader br = null;
		String csvline = "";
		
		try {
			br = new BufferedReader(new FileReader(csvfile));
			while((csvline = br.readLine()) != null) {
				String [] info = csvline.split(delim);
				String id = info[0];
				String text = info[1];
				
				Question q = null;
				
				if(info[2].equals("YES_NO")) {
					q = new Question(id, text, Question.TYPE_YES_NO);
					questionmap.put(id, q);
				} else if(info[2].equals("NUMBER")) {
					q = new Question(id, text, Question.TYPE_NUMBER);
					questionmap.put(id, q);
				} else if(info[2].equals("MULTIPLE")) {
					q = new Question(id, text, Question.TYPE_MULTCHOICE, Arrays.copyOfRange(info, 3, info.length));
					questionmap.put(id, q);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (QuestionException e) {
			e.printStackTrace();
		} finally {
			if(br!=null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public Collection<Question> getQuestions() {
		
		return questionmap.values();
	}

	@Override
	public Collection<Recommendation> getRecommendations() {
		return recmap.values();
	}

	@Override
	public Question getQuestion(String id) {
		
		return questionmap.get(id);
	}
	
	@Override
	public void addRecommendation(Recommendation rec) {
		recmap.put(rec.getId(), rec);
	}

	@Override
	public void clearRecommendations() {
		recmap.clear();
	}
	
	@Override
	public Recommendation getRecommendation(String id) {
		return recmap.get(id);
	}

}
