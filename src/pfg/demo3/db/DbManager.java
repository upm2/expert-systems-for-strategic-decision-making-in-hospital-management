/**
 * <h1> Documentation Database Manager </h1>
 * 
 * Connects to an SQLite database and provides the user with documentation according to the recommendations they have.
 * 
 * @author Glenn AWB
 * @version 1.0
 */
package pfg.demo3.db;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;
import java.util.TreeSet;

public class DbManager {
	
	String url;
	String dbname;
	
	/**
	 * Constructor of Java object that connects to a SQLite database in order to provide the user with documentation.
	 * 
	 * @param url the location of the database
	 * @param dbname the name of the database
	 */
	public DbManager(String url, String dbname) {
		this.url = url;
		this.dbname = dbname;
	}
	
	/**
	 * Returns all the Document IDs that match a certain recommendation.
	 * 
	 * @param recId the ID of the recommendation.
	 * @return a collection of all the IDs of documents that match the recommendation.
	 * @throws SQLException if there is an error querying the database
	 */
	public Set<String> getIdsFromRecommendation(String recId) throws SQLException {

		Connection conn = null;
		TreeSet<String> ids = new TreeSet<String>();
		String sql = "SELECT doc_id FROM rec_doc WHERE rec_id = ?";
		try {
			conn = connect();
			PreparedStatement pstmt = conn.prepareStatement(sql);
			// set the corresponding parameter
            pstmt.setString(1, recId);
            ResultSet rs  = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
            	ids.add(rs.getString("doc_id"));
            }
		}finally {
			if (conn!=null) {
				conn.close();
			}
		}
		return ids;
	}

	/**
	 * Returns the type of document that matches a certain ID.
	 * 
	 * @param id the ID of the document queried
	 * @return a String with the type (file extension)
	 * @throws SQLException if there is an error querying the database
	 */
	public String getType(String id) throws SQLException {
		Connection conn = null;
		String name = "";
		String sql = "SELECT type FROM documents WHERE id = ?";
		try {
			conn = connect();
			PreparedStatement pstmt = conn.prepareStatement(sql);
			// set the corresponding parameter
            pstmt.setString(1, id);
            ResultSet rs  = pstmt.executeQuery();
            // Get the name
            if(rs.next()) {
            	name = rs.getString("type").toUpperCase();
            }
		}finally {
			if (conn!=null) {
				conn.close();
			}
		}
		return name;
	}

	/**
	 * Returns the URL of a webpage in the documentation according to its ID.
	 * 
	 * @param id the ID of the webpage queried
	 * @return a String containing the URL of the webpage or an empty String if there is none
	 * @throws SQLException if there is an error querying the database
	 */
	public String getUrl(String id) throws SQLException {
		Connection conn = null;
		String name = "";
		String sql = "SELECT link FROM documents WHERE id = ?";
		try {
			conn = connect();
			PreparedStatement pstmt = conn.prepareStatement(sql);
			// set the corresponding parameter
            pstmt.setString(1, id);
            ResultSet rs  = pstmt.executeQuery();
            // Get the name
            if(rs.next()) {
            	name = rs.getString("link");
            }
		}finally {
			if (conn!=null) {
				conn.close();
			}
		}
		return name;
	}

	/**
	 * Returns an InputStream with the content of a multimedia document that matches a certain ID.
	 * 
	 * @param id id the ID of the document whose content is queried
	 * @return an InputStream containing the binary contents of the document
	 * @throws SQLException if there is an error querying the database
	 */
	public InputStream getBlob(String id) throws SQLException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		// update sql
        String sql = "SELECT content FROM documents WHERE id = ? ";
        ResultSet rs = null;
        InputStream res = null;
        
        try {
            conn = connect();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	res = rs.getBinaryStream("content");
            }
        }finally {
        	if (conn!=null) {
				conn.close();
			}
        }
            
		return res;
	}
	
	/**
	 * Connects to the database.
	 * 
	 * @return the Connection to the database.
	 * @throws SQLException if there is an error connecting to the database.
	 */
	private Connection connect() throws SQLException {
        
		Connection conn = null;
        // db parameters
    	String url_file = url + dbname;
        // create a connection to the database
        conn = DriverManager.getConnection(url_file);
        
        //System.out.println("Connection to SQLite has been established.");
            
        return conn;
    }

	/**
	 * Returns the name of a document that matches a certain ID.
	 * 
	 * @param id the ID of the document whose name is queried
	 * @return a String containing the name of the document or an empty String if there is none
	 * @throws SQLException if there is an error querying the database
	 */
	public String getName(String id) throws SQLException {
		Connection conn = null;
		String name = "";
		String sql = "SELECT name FROM documents WHERE id = ?";
		try {
			conn = connect();
			PreparedStatement pstmt = conn.prepareStatement(sql);
			// set the corresponding parameter
            pstmt.setString(1, id);
            ResultSet rs  = pstmt.executeQuery();
            // Get the name
            if(rs.next()) {
            	name = rs.getString("name");
            }
		}finally {
			if (conn!=null) {
				conn.close();
			}
		}
		return name;
	
	}
}
