/**
 * <h1> Main Program Window </h1>
 * Window that displays questions, recommendations and documentation.
 * @author Glenn AWB
 * @version 1.0
 */
package pfg.demo3.gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;
import java.awt.List;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JTabbedPane;

import jess.JessException;
import pfg.demo3.app.App;
import pfg.models.Question;
import pfg.models.Recommendation;

public class MainWindow extends Frame {

	private App app;
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor of the window.
	 * 
	 * @param app The  Application manager that created the window.
	 */
	public MainWindow(App app) {
		
		//super class constructor
		super("MRI Management Expert System");
		
		this.app = app;
		
		this.addWindowListener(new Exit()); //Closes Window

		
		setLayout(new BorderLayout());
		Button bRun = new Button("New Decision Making");
		Button bAnswer = new Button("Answer Question");
		Button bRead = new Button("Open Documentation");
		
		//Three panels in the middle
		Panel qPanel = new Panel(new BorderLayout());
		qPanel.add(new Label("Questions", Label.CENTER), BorderLayout.NORTH);
		QuestionList qList = new QuestionList(app.getQuestions());
		qPanel.add(qList, BorderLayout.CENTER);
		qPanel.add(bAnswer, BorderLayout.SOUTH);
		
		Panel rPanel = new Panel(new BorderLayout());
		rPanel.add(new Label("Recommendations", Label.CENTER), BorderLayout.NORTH);
		RecommendationList rList = new RecommendationList();
		rPanel.add(rList, BorderLayout.CENTER);
		rPanel.add(bRun, BorderLayout.SOUTH);
		
		Panel dPanel = new Panel(new BorderLayout());
		dPanel.add(new Label("Documentation", Label.CENTER), BorderLayout.NORTH);
		DocumentationList dList = new DocumentationList();
		dPanel.add(dList, BorderLayout.CENTER);
		dPanel.add(bRead, BorderLayout.SOUTH);

		JTabbedPane tabs = new JTabbedPane();
		tabs.add("Questions", qPanel);
		tabs.add("Recommendations", rPanel);
		tabs.add("Documentation", dPanel);
		add(tabs, BorderLayout.CENTER);
		
		//Label displayed on the bottom of window
		add(new Label("Expert System for strategic decision making in hospital management."), BorderLayout.SOUTH);
		
		//Action Listeners
		bRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					int c = app.run();
					rList.refresh(app.getRecommendations());
					dList.removeAll();
					if(c==0) {
						createErrorDialog("No Recommendations Yet", "Answer more questions to get some recommendations.").setVisible(true);;
					}
				} catch (JessException e) {
					createErrorDialog("Error Running Program", "There was an error during the decision making.").setVisible(true);
					e.printStackTrace();
				}
			}
			
		});
		
		bAnswer.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				if(qList.getSelectedIndex()!=-1) {
					createAQ(qList).setVisible(true);
				}else {
					createErrorDialog("Select a Question!", "No question was selected.").setVisible(true);
				}
			}
		});
		
		rList.addItemListener(new ItemListener() {

			public void itemStateChanged(ItemEvent arg0) {
				try {
					if(rList.getSelectedIndex()!=-1) {
						dList.refresh(app, rList.getSelectedItem());
					}
				} catch (SQLException e) {
					createErrorDialog("Database Error", "There was an error accessing the database.").setVisible(true);
					e.printStackTrace();
				}
				
			}
			
		});
		
		bRead.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				if(dList.getSelectedIndex()!=-1) {
					try {
						String docid = dList.getSelectedItem();
						String type = app.getDocType(docid);
						if(type.equals("URL")) {
							app.openWeb(docid);
						}else if(!type.equals("")) {
							app.openBlob(docid);
						}
					} catch (SQLException e) {
						createErrorDialog("Database Error", "There was an error accessing the database.").setVisible(true);
						e.printStackTrace();
					} catch (IOException e) {
						createErrorDialog("IO Error", "There was an error opening the web page.").setVisible(true);
						e.printStackTrace();
					} catch (URISyntaxException e) {
						createErrorDialog("URL Error", "There was an error in the URL from the database.").setVisible(true);
						e.printStackTrace();
					}
				}else {
					createErrorDialog("Select Documentation!", "No document was selected.").setVisible(true);
				}
				
			}
			
		});
		
		qList.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent evt) {
		        if (evt.getClickCount() == 2) {

		            // Double-click detected
		        	if(qList.getSelectedIndex()!=-1) {
						createAQ(qList).setVisible(true);
					}else {
						createErrorDialog("Select a Question!", "No question was selected.").setVisible(true);
					}
		            
		        }
		    }
		});
		
		rList.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent evt) {
		        if (evt.getClickCount() == 2) {

		            // Double-click detected
		        	if(rList.getSelectedIndex()!=-1) {
						tabs.setSelectedIndex(2);
					}
		            
		        }
		    }
		});
		
		dList.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent evt) {
		        if (evt.getClickCount() == 2) {

		            // Double-click detected
		        	if(dList.getSelectedIndex()!=-1) {
						try {
							String docid = dList.getSelectedItem();
							String type = app.getDocType(docid);
							if(type.equals("URL")) {
								app.openWeb(docid);
							}else if(!type.equals("")) {
								app.openBlob(docid);
							}
						} catch (SQLException e) {
							createErrorDialog("Database Error", "There was an error accessing the database.").setVisible(true);
							e.printStackTrace();
						} catch (IOException e) {
							createErrorDialog("IO Error", "There was an error opening the web page.").setVisible(true);
							e.printStackTrace();
						} catch (URISyntaxException e) {
							createErrorDialog("URL Error", "There was an error in the URL from the database.").setVisible(true);
							e.printStackTrace();
						}
					}else {
						createErrorDialog("Select Documentation!", "No document was selected.").setVisible(true);
					}
		            
		        }
		    }
		});
		
		MenuBar menuBar = new MenuBar();
		
		Menu menuApp = new Menu("Application");
		Menu menuAbout = new Menu("About");
		menuBar.add(menuApp);
		menuBar.add(menuAbout);
		
		//App menu
		MenuItem miAnswer = new MenuItem("Answer Question");
		miAnswer.addActionListener(bAnswer.getActionListeners()[0]);
		menuApp.add(miAnswer);
		MenuItem miRun = new MenuItem("New Decision Making");
		miRun.addActionListener(bRun.getActionListeners()[0]);
		menuApp.add(miRun);
		MenuItem miRead = new MenuItem("Open Documentation");
		miRead.addActionListener(bRead.getActionListeners()[0]);
		menuApp.add(miRead);
		
		//About menu
		MenuItem miAbout = new MenuItem("Help");
		miAbout.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				createAbout().setVisible(true);
			}
			
		});
		menuAbout.add(miAbout);
		
		setMenuBar(menuBar);
		
		//and finally, setting the size...
		pack();
		setSize(1250, 600);
	}
	
	App getApp() {
		return app;
	}
	
	private AnswerQuestion createAQ(QuestionList qList) {
		return new AnswerQuestion(this, qList);
	}
	
	private ErrorDialog createErrorDialog(String header, String message) {
		return new ErrorDialog(this, header, message);
	}

	private About createAbout() {
		return new About(this);
	}
	
} //end of MainWindow class

/**
 * <h1> List of questions to be displayed to the user in the main window. </h1>
 * 
 * @author Glenn AWB
 * @version 1.0
 */
class QuestionList extends List{

	private static final long serialVersionUID = 1L;
	private ArrayList<String> qIds;
	
	/**
	 * Constructor of the list.
	 * @param questions the questions to be displayed.
	 */
	public QuestionList(Collection<Question> questions) {
		setMultipleMode(false);
		qIds = new ArrayList<String>();
		refresh(questions);	
	}
	
	/**
	 * Refreshes the question list with new questions.
	 * 
	 * @param questions the new questions to be displayed.
	 */
	public void refresh(Collection<Question> questions){
		int index = getSelectedIndex();
		removeAll();
		qIds.clear();
		int i = 0;
		for(Question q : questions) {
			
			if(q.getAnswered()) {
				add(q.getQuestion() + " ("+q.getAnswer()+")", i);
			}else {
				add(q.getQuestion() + " (Not answered yet!)", i);
			}
			qIds.add(i, q.getId());
			i++;
		}
		if(index!=-1) {
			select(index);
		}
		
	}
	
	/**
	 * Returns the selected item's ID.
	 * @return a String containing the ID of the question currently selected, or null if no question has been selected.
	 */
	@Override
	public String getSelectedItem() {
		if(getSelectedIndex()!=-1) {
			return qIds.get(getSelectedIndex());
		}else {
			return null;
		}
	}
	
	
}

/**
 * <h1> List of recommendations to be displayed to the user in the main window. </h1>
 * 
 * @author Glenn AWB
 * @version 1.0
 */
class RecommendationList extends List{

	private static final long serialVersionUID = 1L;
	ArrayList<String> rIds;
	
	/**
	 * Constructor of the list.
	 */
	public RecommendationList() {
		setMultipleMode(false);
		rIds = new ArrayList<String>();
	}
	
	/**
	 * Refreshes the question list with new recommendations.
	 * 
	 * @param questions the new recommendations to be displayed.
	 */
	public void refresh(Collection<Recommendation> recs) {
		removeAll();
		int i = 0;
		rIds.clear();
		for(Recommendation r : recs) {
			add(r.getText());
			rIds.add(i, r.getId());
			i++;
		}
		
		
	}
	
	/**
	 * Returns the selected item's ID.
	 * @return a String containing the ID of the recommendation currently selected, or null if no document has been selected.
	 */
	@Override
	public String getSelectedItem() {
		
		if(getSelectedIndex()!=-1) {
			return rIds.get(getSelectedIndex());
		}else {
			return null;
		}
	}	
}

/**
 * <h1> List of recommendations to be displayed to the user in the main window. </h1>
 * 
 * @author Glenn AWB
 * @version 1.0
 */
class DocumentationList extends List{

	private static final long serialVersionUID = 1L;
	ArrayList<String> dIds;
	
	/**
	 * Constructor of the list.
	 */
	public DocumentationList() {
		setMultipleMode(false);
		dIds = new ArrayList<String>();
	}
	
	/**
	 * Refreshes the list with new documentation.
	 * 
	 * @param app the App that created the main window
	 * @param rId the ID of the recommendation whose documents are to be displayed.
	 * @throws SQLException if there is an error consulting the database.
	 */
	public void refresh(App app, String rId) throws SQLException{
		
		removeAll();
		dIds.clear();
		
		//get all the Document IDs that match the Recommendation ID, and add them to the ArrayList
		dIds.addAll(app.getDocIdsfromRecId(rId));
		
		//Create the list to display with the types
		for(String s : dIds) {
			add(app.getDocName(s));
		}
		
	}
	
	/**
	 * Returns the selected item's ID.
	 * @return a String containing the ID of the document currently selected, or null if no document has been selected.
	 */
	@Override
	public String getSelectedItem() {
		
		if(getSelectedIndex()!=-1) {
			return dIds.get(getSelectedIndex());
		}else {
			return null;
		}
	}	
}

/**
 * <h1> Exit </h1>
 * 
 * Window Adapter and Action listener used  to close the system.
 * 
 * @author Glenn AWB
 *
 */
class Exit extends WindowAdapter implements ActionListener{
	
	/**
	 * Invoked when an action occurs.
	 * @param ae A low-level event that indicates an action has occured.
	 */
	@Override
	public void  actionPerformed(ActionEvent ae){		
		System.exit(0);
	}
	
	/**
	 * Invoked when a window is in the process of being closed.
	 * @param we A low-level event that indicates that a window has changed its status. 
	 */
	@Override
	public void windowClosing(WindowEvent we){
		System.exit(0);
	}
	
}//end of Exit class
