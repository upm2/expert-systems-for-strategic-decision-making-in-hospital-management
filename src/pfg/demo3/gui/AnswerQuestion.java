/**
 * <h1> Displays a window used to answer a determined question. </h1>
 * 
 * @author Glenn AWB
 */
package pfg.demo3.gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Choice;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import pfg.models.Question;
import pfg.models.QuestionException;

public class AnswerQuestion extends Dialog{

	private static final long serialVersionUID = 1L;
	
	private Choice answerlist;
	private TextArea number;
	private TextArea qtext;
	
	/**
	 * The Dialog used to answer a question.
	 * 
	 * @param f the Main Window that creates this Dialog.
	 * @param qlist the Question List from the Main Window.
	 */
	public AnswerQuestion(MainWindow f, QuestionList qlist) {
		
		super(f, "Answer Question", true);
		this.addWindowListener(new CloseDialog(this)); //Closes window
		setLayout(new BorderLayout());
		
		Question question = f.getApp().getQuestion(qlist.getSelectedItem());
		answerlist = new Choice();
		number = new TextArea("", 1, 3, TextArea.SCROLLBARS_NONE);
		
		
		add(new Label(question.getQuestion(), Label.CENTER), BorderLayout.NORTH);
		
		qtext = new TextArea("", 20, 20, TextArea.SCROLLBARS_VERTICAL_ONLY);
		qtext.setEditable(false);
		String t = "";
		if(!question.getAnswered()) {
			t += "Not yet answered.";
		}else {
			t += "Current answer: " + question.getAnswer();
		}
		t += "\n\n";
		
		
		Panel mPanel = new Panel(new BorderLayout());
		if(question.getType() == Question.TYPE_MULTCHOICE || question.getType() == Question.TYPE_YES_NO) {
			t += "Possible answers:\n";
			for(String s : question.getPossibleAnswers()) {
				t += s + "\n";
				answerlist.add(s);
			}
			mPanel.add(answerlist, BorderLayout.SOUTH);
		}else if(question.getType() == Question.TYPE_NUMBER) {
			t += "Type a number:\n";
			mPanel.add(number, BorderLayout.SOUTH);
		}
		
		qtext.setText(t);
		mPanel.add(qtext, BorderLayout.CENTER);
		add(mPanel, BorderLayout.CENTER);
		
		//Buttons
		Button aButton = new Button("Answer");
		aButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				String answer = "";
				if(question.getType() == Question.TYPE_MULTCHOICE || question.getType() == Question.TYPE_YES_NO) {
					answer = answerlist.getSelectedItem();
				}else if(question.getType() == Question.TYPE_NUMBER) {
					answer = number.getText();
				}
				try {
					question.answerQuestion(answer);
					qlist.refresh(f.getApp().getQuestions());
					qtext.setText(getQuestionText(question));
				} catch (QuestionException e) {
					new ErrorDialog(f, "Invalid Answer", e.getMessage()).setVisible(true);
				}
				
			}
			
		});
		Button cButton = new Button("Close");
		cButton.addActionListener(new CloseDialog(this));
	
		Panel bPanel = new Panel();
		bPanel.setLayout(new FlowLayout());
		bPanel.add(aButton);
		bPanel.add(cButton);
	
		add(bPanel, BorderLayout.SOUTH);
		
		this.pack();
	    this.setSize(500, 500);
	    this.setLocation(10, 10);
	    this.setResizable(true);
		
	}

	/**
	 * Gets the question text to be displayed in the dialog.
	 * 
	 * @param q the question being answered.
	 * @return a String containing the formatted question text.
	 */
	private String getQuestionText(Question q) {
		String t = "";
		if(!q.getAnswered()) {
			t += "Not yet answered.";
		}else {
			t += "Current answer: " + q.getAnswer();
		}
		t += "\n\n";
		if(q.getType() == Question.TYPE_MULTCHOICE || q.getType() == Question.TYPE_YES_NO) {
			t += "Possible answers:\n";
			for(String s : q.getPossibleAnswers()) {
				t += s + "\n";
			}
			
		}else if(q.getType() == Question.TYPE_NUMBER) {
			t += "Type a number:\n";
		}
		
		return t;
	}
	
}//end of AnswerQuestion
