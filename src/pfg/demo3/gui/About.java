/**
 * <h1> About Window </h1>
 * 
 * Displays information to the user about how to use the system.
 */
package pfg.demo3.gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;

class About extends Dialog{
	
	private static final long serialVersionUID = 1L;
	
	private String about1 = "Get recommendations by answering questions and running the engine!";
	private String about2 = "Answer questions and click on \"New Decision Making\" to get recommendations.\n"
			+ "Select recommendations to get a list of the associated documentation.";
	private String about3 = "You can read the documentation in the \"Documentation\" tab after selecting a recommendation.";
	
	/**
	 * Constructs the Dialog to display information.
	 * 
	 * @param f the Window that created this dialog.
	 */
	public About(Frame f){
		
		super(f, "How to use the Application", true);
		
		this.addWindowListener(new CloseDialog(this)); //Closes window
		
		Panel p1 = new Panel();
		p1.setLayout(new BorderLayout());
		p1.add(new Label(about1, Label.CENTER), BorderLayout.NORTH);
		p1.add(new Label(about2, Label.CENTER), BorderLayout.CENTER);
		p1.add(new Label(about3, Label.CENTER), BorderLayout.SOUTH);
		this.add(BorderLayout.CENTER, p1);
		
		Button b = new Button("OK");
		b.addActionListener(new CloseDialog(this));
		
		Panel p2 = new Panel();
		p2.add(b);
		
		this.add(BorderLayout.SOUTH, p2);
		
		//Size and some other things
	    pack();
	    setLocation(200, 200);
	    setResizable(false);
		
	}
	
}
