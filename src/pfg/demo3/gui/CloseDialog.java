/**
 * <h1> Java class that determines behaviour of the program when closing a dialog window. </h1>
 * 
 * @author Glenn AWB
 */

package pfg.demo3.gui;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


class CloseDialog extends WindowAdapter implements ActionListener{
	
	private Dialog d;
	
	/**
	 * Constructor of the closer
	 * @param d the Dialog to be closed.
	 */
	public CloseDialog(Dialog d){
		this.d = d;
	}
	
	/**
	 * Invoked when a dialog is in the process of being closed.
	 * @param we A low-level event that indicates that a dialog has changed its status. 
	 */
	@Override
	public void windowClosing(WindowEvent we){
		d.setVisible(false);
		d.dispose();
	}
	
	/**
	 * Invoked when a window is in the process of being closed.
	 * @param we A low-level event that indicates that a window has changed its status. 
	 */
	@Override
	public void actionPerformed(ActionEvent ae){
		d.setVisible(false);
		d.dispose();
	}
	
}