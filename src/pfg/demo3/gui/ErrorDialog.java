/**
 * <h1> Creates a window that displays an error message. </h1>
 * 
 * @author Glenn AWB
 * @version 1.0
 */
package pfg.demo3.gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;

class ErrorDialog extends Dialog{

	private static final long serialVersionUID = 1L;

	/**
	 * Creator of the Error message.
	 * @param f The Frame from which this dialog was created.
	 * @param header a String containing the dialog's header.
	 * @param message a String containing the dialog's message.
	 */
	public ErrorDialog(Frame f, String header, String message){
		
		super(f, header, true);
		
		this.addWindowListener(new CloseDialog(this)); //Closes window
		
		Panel p1 = new Panel();
		p1.setLayout(new FlowLayout());
		p1.add(new Label(message, Label.CENTER));
		this.add(BorderLayout.CENTER, p1);
		
		Button b = new Button("OK");
		b.addActionListener(new CloseDialog(this));
		
		Panel p2 = new Panel();
		p2.add(b);
		
		this.add(BorderLayout.SOUTH, p2);
		
	    this.pack();
	    this.setLocation(300, 300);
	    this.setResizable(false);
		
	}
	
}