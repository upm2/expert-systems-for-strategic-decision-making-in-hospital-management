(import pfg.models.*)
(deftemplate Question (declare(from-class Question)))
(deftemplate Recommendation (declare(from-class Recommendation)))

;;(defrule testnum
    ;;(Question (id "testnum"){numAnswer == 600.00})
    ;;=>
    ;;(add (new Recommendation "Rx" "Success!")))

(defrule ruleR1.1
    "Based on demo sheet."
    ;; If there is a question with the id "q1" and the anwer is "yes"
    ;; and there is a question with the id "q2" and the answer is "yes"
    ;; and there is a question with the id "q4" and the answer is "yes"
    (Question (id "q1")(answer "YES"))
    (Question (id "q2")(answer "YES"))
    (Question (id "q4")(answer "NO"))
    =>
    (add (new Recommendation "R1.1" "Open the scan room for 24h.")))

(defrule ruleR1.2
    "Based on demo sheet."
    ;; similar comment
    (Question (id "q1")(answer "YES"))
    (Question (id "q2")(answer "NO"))
    (Question (id "q3")(answer "HIRE MORE PEOPLE"))
    (Question (id "q4")(answer "NO"))
    =>
    (add (new Recommendation "R1.2" "Open the scan room for 24 hours.")))

(defrule ruleR2
    "Based on demo sheet."
    ;; similar comment
    (Question (id "q1")(answer "NO"))
    =>
    (add (new Recommendation "R2" "Externalise.")))

(defrule ruleR3
    "Based on demo sheet."
    ;; similar comment
    (Question (id "q1")(answer "YES"))
    (Question (id "q2")(answer "YES"))
    (Question (id "q4")(answer "YES"))
    =>
    (add (new Recommendation "R3" "Read this document: [Document about RMI machine and how it is safe to keep it on 24 hours.]")))


